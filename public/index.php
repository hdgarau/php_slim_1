<?php
require __DIR__ . '/../vendor/autoload.php';

use App\Middlewares\RandomWishes;
use App\Middlewares\AddLabelHtml;
use App\Middlewares\RandomGreetings;
use App\Controllers\HelloController;
use Slim\Factory\AppFactory;

$app = AppFactory::create();

$app->get('/hello/{name}', HelloController::class  )
    ->add(new RandomGreetings)
    ->add(new RandomWishes)
    ->add(new AddLabelHtml('pre'));

$app->run();