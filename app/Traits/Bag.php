<?php
namespace App\Traits;

trait Bag
{
    static public function random()
    {
        $all = static::all();
        return $all[array_rand($all)];
    }
}