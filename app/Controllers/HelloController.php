<?php
namespace App\Controllers;


use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class HelloController
{
    public function __invoke(Request $request, Response $response, $args)
    {
        $response->getBody()->write($args['name']);
        return $response;
    }
}