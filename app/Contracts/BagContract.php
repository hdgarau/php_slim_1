<?php
namespace App\Contracts;

interface BagContract
{
    static public function all() : array;
}