<?php
namespace App\Models\Bags;

final class Wishes extends Bag
{
    static public function all() : array
    {
        return [
            'Best regards!','Luck for today!','Go ahead!'
        ];
    }
}