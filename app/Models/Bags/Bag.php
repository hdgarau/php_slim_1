<?php
namespace App\Models\Bags;

use App\Contracts\BagContract;
use App\Traits\Bag as TraitsBag;

abstract class Bag implements BagContract
{
    use TraitsBag;
}