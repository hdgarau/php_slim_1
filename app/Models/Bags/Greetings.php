<?php
namespace App\Models\Bags;

final class Greetings extends Bag
{
    static public function all() : array
    {
        return [
            'Hi','Hello','Wellcome'
        ];
    }
}